require("dotenv").config()
const express = require("express")
const axios = require("axios")

const app = express()

app.use(express.json())

const {
    SERVER_PORT
} = process.env

const port = SERVER_PORT || 3005

app.get("/people/:sort", async (req, res) => {
    const { sort } = req.params

    try {
        let people = await axios.get("https://swapi.dev/api/people/")
        let peopleArr = []
        let peoplePages = Math.ceil(people.data.count / 10)
        peopleArr.push(...people.data.results)

        for(let i = 2; i <= peoplePages; i++) {
            let nextPage = await axios.get(`https://swapi.dev/api/people/?page=${i}`)
            peopleArr.push(...nextPage.data.results)
        }
    
        if(sort === "name") {
            peopleArr.sort((a, b) => {
                let nameA = a.name.toUpperCase()
                let nameB = b.name.toUpperCase()
    
                if(nameA < nameB) {
                    return -1
                }
                else if(nameA > nameB) {
                    return 1
                }
                else {
                    return 0
                }
            })
        }
    
        if(sort === "height") {
            peopleArr.sort((a, b) => {
                return a.height - b.height
            })
        }
    
        if(sort === "mass") {
            peopleArr.sort((a, b) => {
                return a.mass - b.mass
            })
        }

        res.status(200).send(peopleArr)
    }
    catch(err) {
        console.log(err)
        res.sendStatus(500)
    }
})

app.get("/planets", async (req, res) => {
    try {
        let planets = await axios.get("https://swapi.dev/api/planets/")
        let planetsArr = []
        let planetsPages = Math.ceil(planets.data.count / 10)
        planetsArr.push(...planets.data.results)

        for(let i = 2; i <= planetsPages; i++) {
            let nextPage = await axios.get(`https://swapi.dev/api/planets/?page=${i}`)
            planetsArr.push(...nextPage.data.results)
        }

        for(let i = 0; i < planetsArr.length; i++) {
            let residentNames = []

            if(planetsArr[i].residents.length) {
                residentNames = await axios.all(planetsArr[i].residents.map(async (resident) => {
                    let residentName = await axios.get(resident)
                    return residentName.data.name
                }))
            }

            planetsArr[i].residents = residentNames
        }

        res.status(200).send(planetsArr)
    }
    catch(err) {
        console.log(err)
        res.sendStatus(500)
    }
})

try {
    app.listen(port, () => console.log(`Listening on port ${port}`))
}
catch(err) {
    console.log(err)
}